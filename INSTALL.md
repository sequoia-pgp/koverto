# Installation

While there is no system package, follow these instructions:

## Install Rust 1.46.0

Koverto requires at least Rust 1.46.0.
For deployment we recommend using a rust docker image
with a recent rust version.

For development you can install rust with rustup in most distributions.
Debian bullseye ships Rust 1.46.0 by default. So here you can just run:

```
sudo apt install rustc cargo
```

## Install system dependencies

Other dependencies can be installed as packeges:

```
sudo apt install -yqq capnproto clang make pkg-config nettle-dev libssl-dev capnproto libsqlite3-dev libssl1.1
```

## Download the code

```
git clone https://gitlab.com/koverto/koverto
```

## Compile the code

```
cd koverto
cargo build
```

## Run via cli

Run the Mail Transfer Agent (MTA) ie. the process but will be
listening for incoming Emails and store them in $HOME/mail.

```
cargo run --bin koverto -- mta
```

Run the process that will encrypt and/or sign the Emails found in $HOME/mail
and send them to the recipienets.

```
cargo run --bin koverto
```

See [koverto man page] (or `man doc/src/koverto.1.md`).

  [koverto man page]: https://koverto.gitlab.io/koverto/koverto.1.html

## Install

To install systemd services run by user `koverto` run:

```
sudo WITH_SYSTEMD=1 make install
```

## Configuration

There is no need for configuration file.

To set custom configuration see [koverto.toml] (or
`man doc/src/koverto.toml.5.md`).

  [koverto.toml]: https://koverto.gitlab.io/koverto/koverto.toml.5.html

To run the receive-only Mail Transfer Agent (MTA), a certificate file is
needed.

Use [Let's Encrypt](https://letsencrypt.org) to create a valid
certificate file.

For local tests, you can create a self-signed certificate with openssl, eg.:

```
openssl req -nodes -x509 -newkey rsa:2048 -keyout key.pem -subj '/CN=localhost' -out cert.pem
openssl pkcs12 -export -out koverto.pfx -inkey key.pem -in cert.pem
```

ADVICE: Any user can send Email to the receive-only MTA, therefore it is
recommended to run a firewal that will at least restrict the network socket
listening for Email to the IP of the servers sending Email to the server where
`koverto` run.
