FROM rust:slim-buster as dependencies
MAINTAINER juga <juga at riseup dot net>
WORKDIR /usr/koverto
# install all dependencies first so this layer can be reused.
# To update the installed packages run docker build --no-cache=true
RUN DEBIAN_FRONTEND=noninteractive apt update && \
    apt install -yq --no-install-recommends \
        capnproto clang make pkg-config nettle-dev \
        libssl-dev libsqlite3-dev libssl1.1 openssl && \
    apt -yq autoremove && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*
# Cache the dependencies
COPY Cargo.toml Cargo.lock ./
RUN mkdir -p src/bin; touch src/lib.rs; \
  echo "fn main () {}" > src/main.rs; \
  echo "fn main () {}" > src/bin/mail-sink.rs; \
  cargo build --release

FROM dependencies as testing
MAINTAINER juga <juga at riseup dot net>
WORKDIR /usr/koverto
# This will not copy anything in .dockerignore
COPY . .
# make sure koverto package actually gets rebuilt
# otherwise we end up with koverto built with
# no-op mail-sink and koverto binaries
RUN cargo clean -p koverto --release
# For some reason /bin/systemctl is not found when building the docker image
# even if systemd is installed.
# ENV WITH_SYSTEMD 1
ENV RUST_BACKTRACE 1
RUN make build && make install

FROM testing as production
MAINTAINER juga <juga at riseup dot net>
# Remove everything that is not needed
RUN rm -rf /usr/koverto; rm -rf /usr/local/cargo; \
  rm -rf /usr/local/rustup
ENV USER koverto
ENV USER_HOME /srv/koverto
ENV UID 2000
ENV GROUP koverto
ENV GID 2000
ENV STORE ${USER_HOME}/store
ENV MAIL ${USER_HOME}/mail
RUN addgroup --gid ${GID} ${GROUP}
RUN adduser --system --uid ${UID} --ingroup ${GROUP} --home ${USER_HOME} ${USER}
USER ${USER}
WORKDIR ${USER_HOME}
RUN mkdir ${MAIL} ${STORE}
VOLUME ${MAIL} ${STORE} ${USER_HOME}
CMD ["/usr/local/bin/koverto"]
