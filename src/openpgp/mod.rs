//! OpenPGP functions (encrypt, sign, armor) using
//! [sequoia_openpgp](https://docs.sequoia-pgp.org/0.9.0/sequoia_openpgp/index.html).
//!
//! If/when `Sequoia` will implement a high-level API, this module might not be
//! needed.
//! Until them, some of the code is partial copy/paste from
//! [Sequoia guides](https://docs.sequoia-pgp.org/sequoia_guide/)
//! and examples.

use sequoia_openpgp::armor;
use sequoia_openpgp::cert;
use sequoia_openpgp::packet::key;
use sequoia_openpgp::packet::prelude::*;
use sequoia_openpgp::policy;
use sequoia_openpgp::serialize::stream;
use sequoia_openpgp::serialize::stream::padding;
// For as_tsk().serialize()?
use sequoia_openpgp::serialize::Serialize;

use anyhow::{Context, Result};

// Needed for .write_all()
use std::io;
use std::io::Write;
use std::{fmt, fs};

use super::{config, email};

mod store;

pub use store::Store;

/// A wrapper around the cryptographic data
///
/// Serves as a high level api that we can define ourselves.
/// An OpenPGP instance has access to the cryptographic data
/// needed by a particular client.
///
/// Processes incoming emails
/// and returns bodies or signatures
/// for composing the outgoing mails.
///
/// Does not handle the construction of Mime messages.
pub struct OpenPGP {
    secret_key: sequoia_openpgp::Cert,
    keys_store: store::Store,
    policy: policy::StandardPolicy<'static>,
}

impl OpenPGP {
    pub fn new(
        config: &config::Config,
        client: &config::Client,
    ) -> Result<Self> {
        Ok(Self {
            secret_key: store::obtain_secret_key(&client.from, &config)?,
            keys_store: store::Store::new(&config.store_config)?,
            policy: policy::StandardPolicy::new(),
        })
    }

    /// Inline sign, encrypt and enarmor a text.
    pub fn sign_encrypt(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Result<Output> {
        let recipient = self
            .keys_store
            .obtain_keys(&recipient)
            .context("No usable encryption key")?;
        let keyid = recipient.keyid().to_string();
        // Note: One certificate may contain several suitable encryption keys.
        let recipients = recipient
            .keys()
            .with_policy(&self.policy, None)
            .alive()
            .revoked(false)
            .for_transport_encryption();
        let signing_keypair = self.signing_key().into_keypair()?;
        let mut sink = vec![];
        let message = stream::Message::new(&mut sink);
        let message = stream::Armorer::new(message).build()?;
        let message =
            stream::Encryptor::for_recipients(message, recipients).build()?;
        let message = padding::Padder::new(message).build()?;
        let message = stream::Signer::new(message, signing_keypair)
            .add_intended_recipient(&recipient)
            .build()?;
        let mut message = stream::LiteralWriter::new(message).build()?;
        message
            .write_all(&source.plaintext().as_string().as_bytes())
            .context("Failed to sign and encrypt")?;
        message.finalize()?;
        let new_body = String::from_utf8(sink)
            .context("Invalid utf8 characters in OpenPGP armor")?;
        trace!("body: {}", new_body);
        Ok(Output::new(new_body, Some(keyid)))
    }

    pub fn sign(&self, source: &email::Incoming) -> Result<Output> {
        let signing_keypair = self.signing_key().into_keypair()?;
        let mut sink = vec![];
        let message = stream::Message::new(&mut sink);
        let message = stream::Armorer::new(message).build()?;
        let mut message = stream::Signer::new(message, signing_keypair)
            .detached()
            .build()?;
        message
            .write_all(&source.body().as_bytes())
            .context("Failed to sign")?;
        message.finalize()?;
        let signature = String::from_utf8(sink)
            .context("Invalid utf8 characters in OpenPGP armor")?;
        trace!("signature: {}", signature);
        Ok(Output::new(signature, None))
    }

    fn signing_key(&self) -> Key<key::SecretParts, key::UnspecifiedRole> {
        self.secret_key
            .keys()
            .secret()
            .with_policy(&self.policy, None)
            .alive()
            .revoked(false)
            .for_signing()
            .next()
            .unwrap()
            .key()
            .clone()
    }
}

pub struct Output {
    pub content: String,
    pub encryption_key: Option<String>,
}

impl Output {
    pub fn new(content: String, encryption_key: Option<String>) -> Self {
        Self {
            content,
            encryption_key,
        }
    }

    pub fn encryption_key_str(&self) -> &str {
        self.encryption_key
            .as_ref()
            .map(|string| string.as_str())
            .unwrap_or("none")
    }
}

impl fmt::Display for Output {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "with encryption key: {}", self.encryption_key_str(),)
    }
}

fn create_file<S>(file_path: S) -> Result<Box<dyn io::Write>>
where
    S: AsRef<str>,
{
    Ok(Box::new(
        fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(file_path.as_ref())?,
    ))
}

/// Generates a signing-capable key and stores it in the file system
///
/// TODO: store it in private sequoia store when it is implemented.
pub fn generate_signing_key<S>(
    sender: S,
    cfg: &config::Config,
) -> Result<sequoia_openpgp::Cert>
where
    S: AsRef<str>,
{
    let sender = sender.as_ref();
    let (cert, revocation) = cert::CertBuilder::new()
        .add_userid(sender)
        .add_signing_subkey()
        .generate()?;

    // Save the key
    let cert_path = cfg.secret_key_path(sender);
    trace!("cert path {}", cert_path.to_string_lossy());
    let w = create_file(&cert_path.to_str().unwrap())?;
    let mut w = armor::Writer::new(w, armor::Kind::SecretKey)?;
    cert.as_tsk().serialize(&mut w)?;
    w.finalize()?;

    // Save the revocation certificate.
    let rev_path = cfg.data_path().join(format!("{}.rev", sender));
    trace!("rev path {}", rev_path.to_string_lossy());
    let w = create_file(&rev_path.to_str().unwrap())?;
    let mut w = armor::Writer::new(w, armor::Kind::Signature)?;
    sequoia_openpgp::Packet::Signature(revocation).serialize(&mut w)?;
    w.finalize()?;
    Ok(cert)
}

#[cfg(test)]
mod tests {

    use std::collections::HashMap;
    // for .read_to_string();
    use core::convert::TryFrom;
    use sequoia_openpgp::crypto::{KeyPair, SessionKey};
    use sequoia_openpgp::packet;
    use sequoia_openpgp::parse::{stream, Parse};
    use sequoia_openpgp::types::KeyFlags;
    use sequoia_openpgp::types::SymmetricAlgorithm;
    use std::io::Read;

    use super::*;
    use crate::tests_common::fixture;
    use anyhow::anyhow;

    const MESSAGE: &str = "Test body.\n";

    fn dearmor(
        sink: &mut (dyn Read + Send + Sync),
    ) -> Result<(Vec<u8>, Option<armor::Kind>)> {
        let mut reader = armor::Reader::new(sink, None);
        let mut dearmored = Vec::<u8>::new();
        reader.read_to_end(&mut dearmored)?;
        Ok((dearmored, reader.kind()))
    }

    struct Helper<'a>(&'a sequoia_openpgp::Cert);

    impl<'a> stream::VerificationHelper for Helper<'a> {
        fn get_certs(
            &mut self,
            _: &[sequoia_openpgp::KeyHandle],
        ) -> Result<Vec<sequoia_openpgp::Cert>> {
            Ok(vec![self.0.clone()])
        }

        fn check(
            &mut self,
            structure: stream::MessageStructure,
        ) -> Result<()> {
            if let stream::MessageLayer::SignatureGroup { ref results } =
                structure.iter().next().unwrap()
            {
                results.get(0).unwrap().as_ref().unwrap();
                Ok(()) /* good */
            } else {
                panic!()
            }
        }
    }

    /// This helper provides secrets for the decryption, fetches public
    /// keys for the signature verification and implements the
    /// verification policy.
    struct HHelper {
        keys: HashMap<sequoia_openpgp::KeyID, KeyPair>,
        verification_cert: sequoia_openpgp::Cert,
    }

    impl HHelper {
        /// Creates a HHelper for the given TPKs with appropriate secrets.
        fn new(
            decrypt_with: sequoia_openpgp::Cert,
            verify_with: sequoia_openpgp::Cert,
            policy: &policy::StandardPolicy,
        ) -> Self {
            HHelper {
                keys: cert_to_hash_map(decrypt_with, policy),
                verification_cert: verify_with,
            }
        }
    }

    fn cert_to_hash_map(
        cert: sequoia_openpgp::Cert,
        policy: &policy::StandardPolicy,
    ) -> HashMap<sequoia_openpgp::KeyID, KeyPair> {
        cert.keys()
            .with_policy(policy, None)
            .alive()
            .revoked(false)
            .key_flags(
                KeyFlags::empty()
                    .set_storage_encryption()
                    .set_transport_encryption(),
            )
            .map(|amalgamation| amalgamation.key())
            .map(|key| {
                key.parts_as_secret().expect("no secret key found").clone()
            })
            .map(|key| {
                (
                    key.keyid(),
                    key.into_keypair()
                        .expect("could not turn key into keypair"),
                )
            })
            .collect()
    }

    impl stream::DecryptionHelper for HHelper {
        fn decrypt<D>(
            &mut self,
            pkesks: &[packet::PKESK],
            _skesks: &[packet::SKESK],
            sym_algo: Option<SymmetricAlgorithm>,
            mut decrypt: D,
        ) -> Result<Option<sequoia_openpgp::Fingerprint>>
        where
            D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
        {
            // Try each PKESK until we succeed.
            for pkesk in pkesks {
                if let Some(pair) = self.keys.get_mut(pkesk.recipient()) {
                    if pkesk
                        .decrypt(pair, sym_algo)
                        .map(|(algo, sk)| decrypt(algo, &sk))
                        .unwrap_or(false)
                    {
                        // TODO: return fp
                        return Ok(None);
                    }
                }
            }
            Err(anyhow!("No key to decrypt message"))
        }
    }

    impl stream::VerificationHelper for HHelper {
        fn get_certs(
            &mut self,
            _ids: &[sequoia_openpgp::KeyHandle],
        ) -> anyhow::Result<Vec<sequoia_openpgp::Cert>> {
            Ok(vec![self.verification_cert.clone()])
        }

        // Signature verification policy for the tests.
        // This is particularly strict to check the data structure we create
        fn check(
            &mut self,
            structure: stream::MessageStructure,
        ) -> sequoia_openpgp::Result<()> {
            for (i, layer) in structure.into_iter().enumerate() {
                match (i, layer) {
                    (0, stream::MessageLayer::Encryption { .. }) => (),
                    (1, stream::MessageLayer::Compression { .. }) => (),
                    (2, stream::MessageLayer::SignatureGroup { results }) => {
                        match results.into_iter().next() {
                            Some(Ok(_)) => return Ok(()),
                            Some(Err(e)) => {
                                return Err(
                                    sequoia_openpgp::Error::from(e).into()
                                )
                            }
                            None => return Err(anyhow!("No signature")),
                        }
                    }
                    (i, layer) => {
                        return Err(anyhow!(format!(
                            "Unexpected message layer {}: {:?}",
                            i, layer
                        )))
                    }
                }
            }
            Err(anyhow!("Signature verification failed"))
        }
    }

    /// Decrypts the given message.
    fn decrypt(
        sink: &mut dyn io::Write,
        ciphertext: &[u8],
        recipient: sequoia_openpgp::Cert,
        sender: sequoia_openpgp::Cert,
    ) -> Result<()> {
        // Make a helper that that feeds the recipient's secret key to the
        // decryptor.
        let policy = &policy::StandardPolicy::new();
        let helper = HHelper::new(recipient, sender, policy);

        // Now, create a decryptor with a helper using the given TPKs.
        let mut decryptor = stream::DecryptorBuilder::from_bytes(ciphertext)?
            .with_policy(policy, None, helper)?;

        // Decrypt the data.
        io::copy(&mut decryptor, sink)?;

        Ok(())
    }

    #[test]
    fn decrypt_verify_encrypted_signed() -> Result<()> {
        let home = fixture::tmp_home_with_keys()?;
        let cfg = config::Config::from(&home);
        let client =
            &cfg.clients.first().context("theres should be a client")?;
        let openpgp = OpenPGP::new(&cfg, &client)?;
        let incoming = incoming_email();
        let output = openpgp.sign_encrypt(&incoming, "user@localhost")?;
        let cert = user_secret_key();
        let mut enarmored = output.content.as_bytes();

        let (cipherbytes, _kind) = dearmor(&mut enarmored)?;

        // Make a helper that that feeds the recipient's secret key to the
        // decryptor.
        let policy = &policy::StandardPolicy::new();
        let sender_key = server_secret_key();
        let helper = HHelper::new(cert, sender_key, policy);
        let mut decryptor =
            stream::DecryptorBuilder::from_bytes(&cipherbytes)?
                .with_policy(policy, None, helper)?;

        // Decrypt the message.
        let mut plainbytes = Vec::new();
        io::copy(&mut decryptor, &mut plainbytes)?;
        let s = String::from_utf8_lossy(&plainbytes);
        assert_eq!(s, incoming.plaintext().as_string());
        Ok(())
    }

    #[test]
    fn decrypt_verify_file() -> Result<()> {
        // Open the armored encrypted message
        let mut email_body_signed_encrypted_enarmored = fs::File::open(
            "tests/data/email_signed_encrypted_from_app_to_user.eml",
        )?;

        // Dearmor the message
        let (cipherbytes, _kind) =
            dearmor(&mut email_body_signed_encrypted_enarmored)?;

        // Decrypt the message.
        let recipient = user_secret_key();
        let sender = server_secret_key();
        let mut plainbytes = Vec::new();
        decrypt(&mut plainbytes, &cipherbytes, recipient, sender).unwrap();
        assert_eq!(String::from_utf8(plainbytes)?, MESSAGE);
        Ok(())
    }

    #[test]
    fn dearmor_file() -> Result<()> {
        let mut f = fs::File::open("tests/data/body_signature.asc")?;
        let (_dearmored, kind) = dearmor(&mut f)?;
        assert_eq!(kind, Some(armor::Kind::Signature));
        Ok(())
    }

    #[test]
    fn verify_signed() -> Result<()> {
        let home = fixture::tmp_home_with_keys()?;
        let cfg = config::Config::from(&home);
        let client =
            &cfg.clients.first().context("theres should be a client")?;
        let openpgp = OpenPGP::new(&cfg, &client)?;
        let incoming = incoming_email();
        let output = openpgp.sign(&incoming)?;
        let detached_sign = output.content.as_bytes();

        // Verify the signature
        let policy = &policy::StandardPolicy::new();
        let key = server_secret_key();
        let mut verifier =
            stream::DetachedVerifierBuilder::from_bytes(&detached_sign)?
                .with_policy(policy, None, Helper(&key))?;

        assert!(verifier.verify_bytes(MESSAGE.as_bytes()).is_ok());
        Ok(())
    }

    #[test]
    fn sign_encrypt() -> Result<()> {
        let home = fixture::tmp_home_with_keys()?;
        let cfg = config::Config::from(&home);
        let client =
            &cfg.clients.first().context("theres should be a client")?;
        let openpgp = OpenPGP::new(&cfg, &client)?;
        let incoming = incoming_email();
        let output = openpgp.sign_encrypt(&incoming, "user@localhost")?;
        assert_eq!(
            output.encryption_key,
            Some("2547 06A7 DF3D BE88".to_owned())
        );
        Ok(())
    }

    fn incoming_email() -> email::Incoming {
        let file = fixture::tests_data_join("email_plain_single.eml");
        let string = fs::read_to_string(&file).unwrap();
        email::Incoming::new(string).unwrap()
    }

    fn user_secret_key() -> sequoia_openpgp::Cert {
        let pile = sequoia_openpgp::PacketPile::from_file(
            fixture::tests_home_join("user@localhost_secret.pgp"),
        )
        .unwrap();
        sequoia_openpgp::Cert::try_from(pile).unwrap()
    }

    fn server_secret_key() -> sequoia_openpgp::Cert {
        let pile =
            sequoia_openpgp::PacketPile::from_file(fixture::tests_home_join(
                "application@localhost.localdomain_secret.pgp",
            ))
            .unwrap();
        sequoia_openpgp::Cert::try_from(pile).unwrap()
    }
}
