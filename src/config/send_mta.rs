use crate::config::PortWrapper;
use serde::{Deserialize, Serialize};
use std::fmt;

use crate::constants::{SEND_MTA_DOMAIN, SEND_MTA_PORT};

/// MTA Configuration for sending the processed Emails.
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct SendMTA {
    // TODO: use an struct for domain name?
    /// A domain name or IP address.
    #[serde(default = "default_send_mta_domain")]
    pub domain: String,
    /// A network socket port.
    #[serde(default = "default_send_mta_port")]
    pub port: PortWrapper,
    #[serde(default)]
    pub credentials: Option<Credentials>,
}

impl SendMTA {
    pub fn to_socket_addr(&self) -> String {
        format!("{}:{}", self.domain.as_str(), self.port)
    }
}

impl Default for SendMTA {
    fn default() -> Self {
        SendMTA {
            domain: default_send_mta_domain(),
            port: default_send_mta_port(),
            credentials: None,
        }
    }
}

impl fmt::Display for SendMTA {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string_pretty(&self).unwrap())
    }
}

fn default_send_mta_domain() -> String {
    SEND_MTA_DOMAIN.to_owned()
}

fn default_send_mta_port() -> PortWrapper {
    PortWrapper(SEND_MTA_PORT)
}

/// Credentials (username and password). They can not have default
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Credentials {
    pub username: String,
    pub password: String,
}

impl fmt::Display for Credentials {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let temp = Credentials {
            username: self.username.clone(),
            password: "********".to_string(),
        };
        write!(f, "{}", serde_json::to_string_pretty(&temp).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn credentials_display() {
        let c = Credentials {
            username: "user".to_string(),
            password: "secret".to_string(),
        };
        // println!("{}", c);
        assert!(!c.to_string().contains("secret"));
    }
}
