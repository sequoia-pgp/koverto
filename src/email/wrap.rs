//! Turn an incoming email into signed and possibly encrypted emails.
//!
//! Creates one signed (and evtl. encrypted) email per recipient.

use crate::email::{Error, Prepare};
use crate::{config, email, openpgp};
use anyhow::{Context, Result};
use rand::Rng;
use std::collections::HashMap;

const BOUNDARY_LENGTH: usize = 30;

pub struct Wrap {
    crypto_action: config::CryptoAction,
    send_all_or_none: bool,
    from: config::ClientFromWrapper,
    openpgp: openpgp::OpenPGP,
}

impl Wrap {
    pub fn new(cfg: &config::Config, client: &config::Client) -> Result<Self> {
        let crypto_action = client.crypto_action.clone();
        debug!("Selected action: {:?}", crypto_action);
        let openpgp = openpgp::OpenPGP::new(cfg, client)?;
        Ok(Self {
            send_all_or_none: client.send_all_or_none,
            from: client.from.to_owned(),
            crypto_action,
            openpgp,
        })
    }
}

impl Prepare for Wrap {
    /// Process the incoming email and prepare the sendable emails.
    ///
    /// It logs the errors and still returns them.
    ///
    fn prepare(&self, email: String) -> Result<Vec<lettre::SendableEmail>> {
        let incoming = email::Incoming::new(email)?;
        if incoming.from() != &self.from {
            return Err(Error::NoAction.into());
        }
        if self.send_all_or_none {
            self.all_or_none(incoming)
                .context("No email sent cause at least one recipient failed")
        } else {
            Ok(self.available_emails(incoming))
        }
    }
}

impl Wrap {
    fn all_or_none(
        &self,
        incoming: email::Incoming,
    ) -> Result<Vec<lettre::SendableEmail>> {
        incoming
            .recipients()
            .into_iter()
            .map(|recipient| self.build_with_context(&incoming, &recipient))
            .collect()
    }

    fn available_emails(
        &self,
        incoming: email::Incoming,
    ) -> Vec<lettre::SendableEmail> {
        incoming
            .recipients()
            .into_iter()
            .map(|recipient| self.build_with_context(&incoming, &recipient))
            .filter_map(|build_result| match build_result {
                Ok(sendable_email) => {
                    trace!("Successfully built email.");
                    Some(sendable_email)
                }
                Err(e) => {
                    warn!("{:#}", e);
                    None
                }
            })
            .collect()
    }

    /// Constructs an outgoing email for the given recipient.
    ///
    /// Takes care of the cryptographic operations on the mail body
    /// and constructs the mime message.
    ///
    /// Picks the cryptographic actions to perform
    /// based on the selected `CryptoAction`.
    fn build_with_context(
        &self,
        incoming: &email::Incoming,
        recipient: &str,
    ) -> Result<lettre::SendableEmail> {
        trace!("Building email to recipient {}", &recipient);
        self.build(&incoming, &recipient)
            .context(format!("When composing to {}.", &recipient))
    }

    fn build(
        &self,
        incoming: &email::Incoming,
        recipient: &str,
    ) -> Result<lettre::SendableEmail> {
        let email = self.builder(&incoming, &recipient)?;
        let from = lettre::EmailAddress::new(incoming.from().to_string())
            .context(format!("from: {}", incoming.from()))?;
        let to = lettre::EmailAddress::new(recipient.to_string())
            .context(format!("to: {}", recipient))?;
        Ok(lettre::SendableEmail::new(
            lettre::Envelope::new(Some(from), vec![to])?,
            self.new_message_id(),
            email.as_string().as_bytes().to_vec(),
        ))
    }

    fn new_message_id(&self) -> String {
        format!("<{}.koverto@localhost>", uuid::Uuid::new_v4())
    }

    fn builder(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Result<::email::MimeMessage> {
        match self.crypto_action {
            config::CryptoAction::MustEncrypt => {
                self.must_encrypt(&source, &recipient)
            }
            config::CryptoAction::Fallback => {
                self.fallback(&source, &recipient)
            }
            config::CryptoAction::SignOnly => {
                self.sign_only(&source, &recipient)
            }
        }
    }

    fn must_encrypt(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Result<::email::MimeMessage> {
        let mut message = Wrap::openpgp_encrypted(
            source,
            self.openpgp.sign_encrypt(source, recipient)?.content,
        );
        message.headers.replace(("To", recipient).into());
        Ok(message)
    }

    fn sign_only(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Result<::email::MimeMessage> {
        // As of Sep 2019, Sequoia always use sha512
        let mut message = Wrap::openpgp_signed(
            source,
            self.openpgp.sign(source)?.content,
            "pgp-sha512",
        );
        message.headers.replace(("To", recipient).into());
        Ok(message)
    }

    fn fallback(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Result<::email::MimeMessage> {
        self.must_encrypt(source, recipient)
            .or_else(|_err| self.sign_only(source, recipient))
    }

    /// Build a multipart/encrypted MIME message as specified in [RFC 1847]
    ///
    /// [RFC 1847]: https://tools.ietf.org/html/rfc1847
    pub fn openpgp_encrypted<S: Into<String>>(
        source: &email::Incoming,
        body_octet_stream: S,
    ) -> ::email::MimeMessage {
        let mut pgp_encrypted = ::email::MimeMessage::new_blank_message();
        pgp_encrypted.body = "Version: 1".to_string();
        pgp_encrypted.headers.insert(
            // XXX: Change to mime::APPLICATION_PGP_ENCRYPTED.to_string(),
            // when #114 is merged
            ("Content-Type", "application/pgp-encrypted").into(),
        );
        pgp_encrypted.headers.insert(
            ("Content-Description", "PGP/MIME version identification").into(),
        );
        let mut octet_stream = ::email::MimeMessage::new_blank_message();
        octet_stream.body = body_octet_stream.into();
        octet_stream.headers.insert(
            ( "Content-Type",
              format!("{}; name=\"encrypted.asc\"",
                      "application/octet-stream")
              // XXX: Change to
              // mime::APPLICATION_OCTET_STREAM.to_string()),
            )
                .into(),
        );
        octet_stream.headers.insert(
            ("Content-Description", "OpenPGP encrypted message").into(),
        );
        octet_stream.headers.insert(
            (
                "Content-Disposition",
                "inline; \
              filename=\"encrypted.asc\"",
            )
                .into(),
        );

        let mut params = HashMap::new();
        params.insert(
            "protocol".to_string(),
            "\"application/pgp-encrypted\"".to_string(),
        );

        let mut message = source.message().clone();
        message.boundary = Wrap::random_boundary();
        let ct = ::email::Header::new(
            "Content-Type".into(),
            format!(
                "multipart/encrypted; \
                    protocol=\"application/pgp-encrypted\"; \
                 boundary=\"{}\"",
                message.boundary
            ),
        );
        message.headers.replace(ct.clone());
        // XXX work around odd bug in email
        message.headers.insert(ct);
        message.body = "This is an OpenPGP/MIME encrypted message \
            (RFC 4880 and 3156)"
            .to_string();
        message.children = vec![pgp_encrypted, octet_stream];
        message
    }

    /// Build a multipart/signed MIME message as specified in [RFC 1847]
    ///
    /// [RFC 1847]: https://tools.ietf.org/html/rfc1847
    pub fn openpgp_signed<T, U>(
        source: &email::Incoming,
        body_signature: T,
        algo: U,
    ) -> ::email::MimeMessage
    where
        T: Into<String>,
        U: Into<String>,
    {
        let mut pgp_signed = ::email::MimeMessage::new_blank_message();
        pgp_signed.body = body_signature.into();
        pgp_signed.headers.insert(
            (
                "Content-Type",
                // XXX: Use mime::APPLICATION_PGP_SIGNATURE
                "application/pgp-signature; name=\"signature.asc\"",
            )
                .into(),
        );
        pgp_signed.headers.insert(
            ("Content-Description", "OpenPGP digital signature").into(),
        );
        pgp_signed.headers.insert(
            (
                "Content-Disposition",
                "attachment; filename=\"signature.asc\"",
            )
                .into(),
        );

        let mut message = source.message().clone();
        message.boundary = Wrap::random_boundary();
        let ct = ::email::Header::new(
            "Content-Type".into(),
            format!(
                "multipart/signed; \
                protocol=\"application/pgp-signed\"; \
                micalg=\"{}\"; \
                boundary=\"{}\"",
                algo.into(),
                message.boundary
            ),
        );
        message.headers.replace(ct.clone());
        // XXX work around odd bug in email
        message.headers.insert(ct);
        message.body = "This is an OpenPGP/MIME signed message \
            (RFC 4880 and 3156)"
            .to_string();
        message.children = vec![source.plaintext(), pgp_signed];
        message
    }

    fn random_boundary() -> String {
        let mut rng = rand::thread_rng();
        std::iter::repeat(())
            .map(|()| rng.sample(rand::distributions::Alphanumeric))
            .take(BOUNDARY_LENGTH)
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests_common::fixture;
    use std::fs;

    #[test]
    fn prepare_encrypt() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let client = &cfg.clients.first().expect("theres should be a client");
        let wrap = Wrap::new(&cfg, client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_ok());
    }

    #[test]
    fn prepare_sign() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let mut client = config::Client::default();
        client.crypto_action = config::CryptoAction::SignOnly;
        let wrap = Wrap::new(&cfg, &client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_ok());
    }

    #[test]
    fn prepare_sign_encrypt() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let mut client = config::Client::default();
        client.crypto_action = config::CryptoAction::MustEncrypt;
        // Sign and encrypt.
        let wrap = Wrap::new(&cfg, &client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_ok());
    }

    #[test]
    fn prepare_err_with_send_all() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let mut client = config::Client::default();
        client.send_all_or_none = true;
        // Encrypt.
        let wrap = Wrap::new(&cfg, &client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_err());
        let expected = "No email sent cause at least one recipient failed: \
                        When composing to foo@localhost.: \
                        No usable encryption key: \
                        Key not found";
        assert_eq!(expected, format!("{:#}", email.err().unwrap()));
    }

    fn incoming_email() -> String {
        fs::read_to_string("tests/data/email_plain.eml").unwrap()
    }

    #[test]
    fn test_encrypted_email() {
        let incoming = email::Incoming::new(incoming_email()).unwrap();
        let message = Wrap::openpgp_encrypted(
            &incoming,
            "-----BEGIN PGP MESSAGE-----\n-----END PGP MESSAGE-----",
        );
        let mail_text = message.as_string();

        let mime = ::email::MimeMessage::parse(&mail_text).unwrap();

        assert_eq!(
            mime.message_type,
            Some(::email::MimeMultipartType::Encrypted),
        );
        assert_eq!(mime.headers.len(), 8, "{:?}", mime.headers);
        // XXX: Extra `\r\n` are added when parsing.
        assert_eq!(
            mime.body,
            "This is an OpenPGP/MIME encrypted message \
            (RFC 4880 and 3156)\r\n",
            "{}",
            mail_text
        );
        assert_eq!(mime.children.len(), 2);
        assert_eq!(
            mime.children[1].body,
            "-----BEGIN PGP MESSAGE-----\n\
                    -----END PGP MESSAGE-----\r\n\r\n"
        );
        assert_eq!(mime.children[0].body, "Version: 1\r\n\r\n");

        let mime2 = ::email::MimeMessage::parse(&mime.as_string()).unwrap();
        assert_eq!(format!("{}\r\n", mime.body), mime2.body);
        assert_eq!(mime.boundary, mime2.boundary);
        assert_eq!(mime.message_type, mime2.message_type);
        assert_eq!(
            format!("{}\r\n\r\n", mime.children[0].body),
            mime2.children[0].body
        );
        assert_eq!(
            format!("{}\r\n\r\n", mime.children[1].body),
            mime2.children[1].body
        );
    }

    #[test]
    fn test_openpgp_signed() {
        let incoming = email::Incoming::new(incoming_email()).unwrap();
        let message = Wrap::openpgp_signed(
            &incoming,
            "-----BEGIN PGP SIGNATURE-----\n-----END PGP SIGNATURE-----",
            "pgp-sha512",
        );
        let mail_text = message.as_string();

        let mime = ::email::MimeMessage::parse(&mail_text)
            .context(format!("Failed to parse: {}", mail_text))
            .unwrap();

        assert_eq!(
            mime.message_type.unwrap(),
            ::email::MimeMultipartType::Signed
        );
        assert_eq!(mime.headers.len(), 8);
        // XXX: Extra `\r\n` are added when parsing.
        assert_eq!(
            mime.body,
            "This is an OpenPGP/MIME signed message \
                   (RFC 4880 and 3156)\r\n"
        );
        assert_eq!(mime.children.len(), 2);
        assert_eq!(mime.children[0].body, "Test body.\n\n\r\n\r\n");
        assert_eq!(
            mime.children[1].body,
            "-----BEGIN PGP SIGNATURE-----\n\
                    -----END PGP SIGNATURE-----\r\n\r\n"
        );

        let mime2 = ::email::MimeMessage::parse(&mime.as_string()).unwrap();
        assert_eq!(format!("{}\r\n", mime.body), mime2.body);
        assert_eq!(mime.boundary, mime2.boundary);
        assert_eq!(mime.message_type, mime2.message_type);
        assert_eq!(
            format!("{}\r\n\r\n", mime.children[0].body),
            mime2.children[0].body
        );
        assert_eq!(
            format!("{}\r\n\r\n", mime.children[1].body),
            mime2.children[1].body
        );
    }
}
