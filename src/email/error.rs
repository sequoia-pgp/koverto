/// Possible errors processing an Email.
#[derive(Debug)]
pub enum Error {
    NoAction,
    EncryptError(String),
    SignError(String),
    SignEncryptError(String),
    NoDateError,
    InvalidDateError,
    NoFromError,
    NoToError,
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::NoAction => write!(f, "No action configured for sender"),
            Error::EncryptError(key) => {
                write!(f, "Error encrypting with key: {}", key)
            }
            Error::SignError(key) => {
                write!(f, "Error signing with key: {}", key)
            }
            Error::SignEncryptError(key) => {
                write!(f, "Error signing and encrypting with key: {}", key)
            }
            Error::NoDateError => write!(f, "No Date header"),
            Error::InvalidDateError => write!(f, "Invalid date header"),
            Error::NoFromError => write!(f, "No From header"),
            Error::NoToError => write!(f, "No To header"),
        }
    }
}
