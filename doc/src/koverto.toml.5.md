koverto.toml - the koverto configuration file
======================================================

DESCRIPTION
-----------

A configuration file in TOML ⟨https://github.com/toml-lang/toml⟩ format
to specify a variety of configuration options for koverto. Resides
at $HOME/.koverto.toml.

FORMAT
------

See the TOML GitHub Repository ⟨https://github.com/toml-lang/toml⟩ for
details about the syntax of the configuration file.

CONFIG
------

The configuration is specified in sections which each have their own
top-level tables ⟨https://github.com/toml-lang/toml\#table⟩ , with
key/value pairs specified in each section.

**Note:** Currently mails will only be processed according to the
first configured client.

Example:

```
log_level = "trace"

[send_mta]
domain = "localhost"
port = 25

[send_mta.credentials]
username = "foo"
password = "bar"

[[clients]
from = "application\@localhost.localdomain"
directory = "mail/must-encrypt"
crypto_action = "MustEncrypt"

[[clients]
from = "application\@localhost.localdomain"
directory = "mail/sign-only"
crypto_action = "SignOnly"

[[clients]
from = "application\@localhost.localdomain"
directory = "mail/fallback"
crypto_action = "Fallback"

[[clients]
from = "application\@localhost.localdomain"
directory = "mail/must-encrypt-all"
crypto_action = "MustEncrypt"
send_all_or_none = true
```

SEE ALSO
--------

**koverto** (1), *https://https://juga0.gitlab.io/koverto/*

BUGS
----

Please report bugs at https://gitlab.com/juga0/koverto/issues

AUTHOR
------

juga [juga at riseup dot net]

COPYRIGHT
---------

GPLv3
