# Koverto requirements analysis

## About Kuvert

Things about Kuvert that are explained in the README and manual pages [Kuvert]
that are important for Koverto design.

### What Kuvert does not

- Does not work as mailman or schleuder, in which the final destinations are
  managed by aliases [mailman].
- In the same way, it does not require to modify an email server setup.

### What Kuvert does

- It requires to modify the configuration of the application
  (from now on `client`) that send emails to be processed by Kuvert.
- Only one `Kuvert` process can run in the same system, because it launches
  a receive-only MTA in a port.
- Only one `.Kuvert` configuration file per `Kuvert` process.
- `kuvert_submit` and `Kuvert` share the same configuration file.
- Only one `.kuvert_queue` directory.
- The client needs to be configured to either call `kuvert_submit` or configure
  the client SMTP credentials with the Kuvert's receive-only MTA.
- `Kuvert` process *only- process emails from the `.kuvert_queue` or incoming
  in the Kuvert's MTA.
- `Kuvert` uses *only- one keyring (GnuPG keyring).
- `Kuvert` delivers the final email via `sendmail` or external email server
  found in the configuration file.
- `Kuvert` allows per-recipient rules, but not per-sender rules
- Kuvert logging in info mode does not say much, in debug mode is too verbose
- Kuvert only allows one sender email and key to sign

For better understanding, see the example
[Kuvert's sessions](kuvert_sessions.md).

## Differences of Sequoia store with GnuPG keyring

- Sequoia store can have different `realm`s (each of them has also an
  associated) label name that contain different sets of public keys.
- Sequoia store does not have private/secret keys so far.
  Therefore, *Umschagend can not sign emails- for now.

## Initial thoughts

### What Koverto could do different to Kuvert

#### Idea 1

- Have different queue directories per configuration or cli argument.
- Have a different sequoia store realm per queue directory.

Problem:

- How would `koverto` process know on which queue directory to search
  for emails or which store realm to search for OpenPGP keys?.
  One way could be to use the email addresses from which the emails come from,
  but there could be several levels of local_parts and subdomains to take into
  account and it adds complexity. In addition we want to be able to use the
  same sender email address with different clients and strategies.

#### Idea 2

- Allow different `koverto` processes to read different configuration files
  and use different queues and sequoia store realms.

Problem:

- Adds complexity and can become confussing to administer.

#### Idea 3

- If the applications only use the receive-only MTA, there is no need for a
  queue directory

#### Idea 4

- Use an ssh tunnel, client or sendmail to put the email notifications in
  a directory read by `koverto`. See the
  [Deployment Impossible](diagrams.md#impossible-deployment) diagram.

Problem:

- The applications do not allow to run arbitrary commands for sending their
  notifications, they only allow an SMTP server.


### Using docker volume mounts

Koverto will most likely run in docker containers using volume mounts.
This way the code lives in the image and the data in volume mounts.
Data notably includes:
* The sequoia store with all the public keys
* The mail queue directories
* log files

Volume mounts can be shared with the host and/or other docker containers.
This way standard sequoia tools can be used to add keys to the keystore.

In addition a receive only MTA such as OpenSMTPD could run in a separate
container and queue the mails to then be processed by koverto.


### Minimal Koverto design

- Make `koverto` behave the same way as `Kuvert`, one configuration file
  per email submission/MTA, process, queue and store realm, to do not add
  complexity and be compatibly with `Kuvert`.
- If different applications would like to have different sequoia store realms,
  they would need to run `koverto` in different systems.
- Use the same configuration variables as `Kuvert`, to make it easy to
  understand/migrate to `koverto` if the users already know/administer
  `Kuvert`.
- Use the same cli arguments as Kuvert, for the same reason.

#### Handle multiple queue dirs with different policies

A single koverto instance could allow
for handling mails according to different policies.

The configuration would set the policy
based on the directory the mails are queued in.

For example koverto could watch for queued mails in
```
queue/must-encrypt
queue/fallback
queue/sign-only
```

This way the MTA could be configured
to store the mails in different folders
based on the requirements of the clients.

It could expose this distinction
by receiving mails on different domains, ports
or with different authentication credentials.

#### Receive mails with koverto itself.

Work has started on using Samotop as an SMTP server
that hands the incoming mails to koverto.

Currently it is running as a receive only MTA
that queues the mails in a directory.

However this integration would be particularly interesting
if we need to couple the MTA and the koverto process
closer together for some reason.

Samotop is a research project and the development
seems stalled for quite a while.
However we could not find a more mature rust SMTPd out there either.

For now using a different MTA to queue the mails
seems to be the way to go for production environments.

### Kuvert and Koverto CLI arguments

```text
    submit  as kuvert_submit. See kuvert_submit for the following arguments:
        -f  As Kuvert.
            Maybe to implement in the future
        -t  As Kuvert, but for koverto is the default right now.
            Maybe to implement in the future.
    Without submcommand, it will run the local MTA, if configured, and
    it will reads the emails in the configured queue.

    -c      Path to the configuration file. This option is not availabe in
            Kuvert.
    -o      As Kuvert, but for koverto it's the default right now.
    -d      As Kuvert. Maybe to implement in the future
    -r      (SIGUSR1) As Kuvert, it should be implemented.
    -k      (SIGTERM) As Kuvert, it needs to be implement,
            to allow stop koverto cleanly and easy systemd integration
```

## Requirements by actual Kuvert users

### How they use Kuvert

- not use `kuvert_submit` command, because the applications they use do not
  allow to use arbitrary commands for notifications, only MTAs.
- send email using the configuration of the Kuvert receive-only MTA.
- Multiple instances of Kuvert in [docker container]s to be able to have have
  different applications using different MTA credentials (ie. not having the
  same password to authenticate with Kuvert receive-only MTA)
- The different applications also require Kuvert do different things, eg.
  - encrypt (and sign?) or drop
  - encrypt opportunistically (and sign?)
  - unencrypted but signed
- An script to reload Kuvert cause it does not detect when new keys are added
  or removed

#### keyring

- all Kuvert instances use the same keyring
- keys that are verified are stored in a git repository with the file named
  with the email address, (eg. alice@example.example.asc?) [pad] L105
  - if a key has multiple verified email addresses, there is one file per
    email address. [pad] L106

### What they need

- have only one application running as a receive-only MTA (#24) that allows
  different credentials, ie. user/password per client
- allow each client to do different things, ie. encrypt and/or sign
- do not have to reload Kuvert to detect changes on the keyring

#### Logging

- At info level, it should log:
  - email received from alice@example.example
  - email sent encrypted to bob@example.example with key BBBBBBBBBBBBBBBB
    and signed with key CCCCCCCCCCCCCCCC
- At debug level:
  - plain text when something (what) fails
- have the option to run Kuvert foreground (#21)

#### Sender rules

- Each application (user/password) send the email with a different `From` and
  therefore possibly using a different key to sign

### What would be nice to have

More ideas not required for a first release in the notes we took in
a [pad] [file](data/public-kuvert-sequoia.txt).

[mailman]: https://www.gnu.org/software/mailman/mailman-install/postfix-integration.html

[Kuvert]: https://github.com/az143/Kuvert/blob/master/README

[docker container]: https://github.com/occrp/Kuvert/blob/master/Dockerfile#L5

[script]: https://github.com/occrp/Kuvert/blob/master/run.sh#L50

[pad]: https://pad.occrp.org/p/public-Kuvert-sequoia/timeslider#2328
