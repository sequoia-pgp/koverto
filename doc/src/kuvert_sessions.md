# Kuvert sessions

## Running kuvert configured as a local MTA

Configure Kuvert and launch it by running `kuvert` without any option.

Look at `/var/log/mail.log`:

```text
Jun 10 08:16:27 machine kuvert[13782]: kuvert version 2.0.13+b1 starting
Jun 10 08:16:27 machine kuvert[13783]: reading keyring...
Jun 10 08:16:29 machine kuvert[13783]: finished reading keyring
Jun 10 08:16:29 machine kuvert[13783]: ignoring uid test@example.example for 0xAAAAAAAA, reason: revoked
```

Then send an email using Kuvert's MTA:

```text
telnet localhost 2587
Trying ::1...
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
220 machine ESMTP Net::Server::Mail (Perl) Service ready
EHLO localhost
250-machine Service ready
250 AUTH LOGIN PLAIN
AUTH LOGIN
334 VXNlcm5hbWU6
dXNlcg==
334 UGFzc3dvcmQ6
c29tZXRoaW5nU0VDUkVU
235 Authentication successful
MAIL FROM:<juga@sequoia-pgp.org>
250 sender juga@sequoia-pgp.org OK
RCPT TO:<neal@sequoia-pgp.org>
250 recipient neal@sequoia-pgp.org OK
DATA
354 Start mail input; end with <CRLF>.<CRLF>
Subject: test
test
.
250 Mail enqueued as 1560240175317267
```

Look at `/var/log/mail.log`:

```text
Jun 11 08:02:55 machine kuvert[32209]: queueing email from juga@sequoia-pgp.org to neal@sequoia-pgp.org
Jun 11 08:03:20 machine kuvert[32209]: finished enqueueing mail 1560240175317267
Jun 11 08:03:50 machine kuvert[32210]: processing 1560240175317267 for kuvertuser
Jun 11 08:03:50 machine kuvert[32210]: Problem with /home/kuvertuser/.kuvert_queue/1560240175317267#012Your mail "/home/kuvertuser/.kuvert_queue/1560240175317267" could not be processed and #012kuvert has given up on it.#012Please review the following error details to determine what went wrong:#012#012#012could not parse From: header!#012#012kuvert has renamed the problematic mail to "/home/kuvertuser/.kuvert_queue/.1560240175317267";#012if you want kuvert to retry, rename it to an all-numeric filename. Otherwise you should delete the file.#012#012Please note that processing may have worked for SOME recipients already!
```

Look at `/home/kuvertuser/.kuvert_queue/.1560240175317267`:

```text
X-Kuvert-From: juga@sequoia-pgp.org
X-Kuvert-To: neal@sequoia-pgp.org
Subject: test
test
```

## Running kuvert_submit

Run:

```text
kuvert&
kuvert_submit < email_file.txt
```

Where `email_file.txt` contains:

```text
To: neal <neal@sequoia-pgp.org>, foo <foo@bar.baz>
Cc: dkg <dkg@aclu.com>, justus@sequoia-pgp.org
From: juga <juga@sequoia-pgp.org>
Subject: Test subject
Date: Wed, 15 May 2019 07:14:00 +0000
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Language: en-US
Content-Transfer-Encoding: 8bit

Test body
```

Look at `/home/kuvertuser/.kuvert_queue/<number>` before the email is
proccesed, it contains the actual email:

```text
To: neal <neal@sequoia-pgp.org>, foo <foo@bar.baz>
Cc: dkg <dkg@aclu.com>, justus@sequoia-pgp.org
From: juga <juga@sequoia-pgp.org>
Subject: Test subject
Date: Wed, 15 May 2019 07:14:00 +0000
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Language: en-US
Content-Transfer-Encoding: 8bit

Test body
```

Look at `/var/log/mail.log`:

```text
Jun 11 09:05:51 machine kuvert[32210]: processing 3173 for user
Jun 11 09:05:51 machine kuvert[32210]: sending mail (signed with 0xF806D46B) to neal@sequoia-pgp.org, foo@bar.baz, dkg@aclu.com, justus@sequoia-pgp.org
Jun 11 09:05:51 machine kuvert[32210]: done handling file 3173
```

After the email has been processed, it's not anymore in the queue.

If you have `exim` configured to submit email only locally, you can see the
email in the queue:

```text
Received: from Debian-exim by machine.localhost.localdomain with local (Exim 4.89)
	id 1hacj1-0000po-IL
	for juga@sequoia-pgp.org; Tue, 11 Jun 2019 09:05:51 +0000
X-Failed-Recipients: neal@sequoia-pgp.org,
  foo@bar.baz,
  dkg@aclu.com,
  justus@sequoia-pgp.org
Auto-Submitted: auto-replied
From: Mail Delivery System <Mailer-Daemon@machine.localdomain>
To: juga@sequoia-pgp.org
Content-Type: multipart/report; report-type=delivery-status; boundary=1560243951-eximdsn-406574487
MIME-Version: 1.0
Subject: Mail delivery failed: returning message to sender
Message-Id: <E1hacj1-0000po-IL@machine.localhost.localdomain>
Date: Tue, 11 Jun 2019 09:05:51 +0000

--1560243951-eximdsn-406574487
Content-type: text/plain; charset=us-ascii

This message was created automatically by mail delivery software.

A message that you sent could not be delivered to one or more of its
recipients. This is a permanent error. The following address(es) failed:

  neal@sequoia-pgp.org
    Mailing to remote domains not supported
  foo@bar.baz
    Mailing to remote domains not supported
  dkg@aclu.com
    Mailing to remote domains not supported
  justus@sequoia-pgp.org
    Mailing to remote domains not supported

--1560243951-eximdsn-406574487
Content-type: message/delivery-status

Reporting-MTA: dns; machine.localhost.localdomain

Action: failed
Final-Recipient: rfc822;justus@sequoia-pgp.org
Status: 5.0.0

Action: failed
Final-Recipient: rfc822;dkg@aclu.com
Status: 5.0.0

Action: failed
Final-Recipient: rfc822;foo@bar.baz
Status: 5.0.0

Action: failed
Final-Recipient: rfc822;neal@sequoia-pgp.org
Status: 5.0.0

--1560243951-eximdsn-406574487
Content-type: message/rfc822

Return-path: <juga@sequoia-pgp.org>
Received: from user by machine.localhost.localdomain with local (Exim 4.89)
	(envelope-from <juga@sequoia-pgp.org>)
	id 1hacj1-0000pm-HS; Tue, 11 Jun 2019 09:05:51 +0000
To: neal <neal@sequoia-pgp.org>, foo <foo@bar.baz>
Cc: dkg <dkg@aclu.com>, justus@sequoia-pgp.org
From: juga <juga@sequoia-pgp.org>
Subject: Test subject
Message-ID: <5d5e11f8-1b2f-ed29-2637-830b2ed3511e@sequoia-pgp.org>
Date: Wed, 15 May 2019 07:14:00 +0000
MIME-Version: 1.0
Content-Type: multipart/signed; boundary="----------=_1560243951-32210-1"; micalg="pgp-sha512"; protocol="application/pgp-signature"

------------=_1560243951-32210-1
Content-Type: text/plain; charset=utf-8
Content-Language: en-US
Content-Transfer-Encoding: quoted-printable

Test body=

------------=_1560243951-32210-1
Content-Type: application/pgp-signature; name="signature.asc"
Content-Disposition: inline; filename="signature.asc"
Content-Transfer-Encoding: 7bit
Content-Description: Digital Signature

-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQReKwnjtGckC35u/P9Mj5CXZKMUIQUCXP9u7wAKCRBMj5CXZKMU
IVAaAP4o3jJh8OJH/uGaLebwWRgTLC+zGYpJv+HXjwK41r2AIQEAkWHrtRSbm1nx
wWMDeWv15sV8B3mvDLfQ+iDN/9VqhgY=
=e2zF
-----END PGP SIGNATURE-----

------------=_1560243951-32210-1--

--1560243951-eximdsn-406574487--
```

Important to note the the email is signed.
