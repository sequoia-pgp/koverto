# User documentation

- [Installation](INSTALL.md)
- [`koverto` Manual page](koverto.1.md)
- [Configuration manual page](koverto.toml.5.md)
